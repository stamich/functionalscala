package io.codeswarm

import cats.Monoid

import scala.annotation.targetName

class MonoidExampleGeneric[A, B] {

  val monoidGeneric: Monoid[A] = new Monoid[A] {

    override def empty: A = ???

    override def combine(x: A, y: A): A = ???
  }

  private def addString(x: A, y: A)(implicit n: Numeric[Double]): Double = {
    val a = x.asInstanceOf[String].toDouble
    val b = y.asInstanceOf[String].toDouble
    n.plus(a, b)
  }

  private def add(x: A, y: A)(implicit n: Numeric[A]): A = n.plus(x, y)
  
  private def stringChecker(x: A): Boolean = {
    if (x.isInstanceOf[String]) true
    else false
  }

  private def matcher(result: A): B = result match {
    case _: Int => result.asInstanceOf[B]
    case _: Float => result.asInstanceOf[B]
    case _: Double => result.asInstanceOf[B]
    case _: Long => result.asInstanceOf[B]
    case _: BigInt => result.asInstanceOf[B]
    case _: BigDecimal => result.asInstanceOf[B]
    case _ => None.asInstanceOf[B]
  }
}
