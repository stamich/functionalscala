package io.codeswarm

import cats.kernel.Monoid

class MonoidExample {
  def monoidString(list: List[String]): String = {
    if (list.nonEmpty) Monoid[String].combineAll(list)
    else Monoid[String].empty
  }

  val monoidStrings: Monoid[String] = new Monoid[String] {
    override def empty: String = ""

    override def combine(x: String, y: String): String = x + y
  }

  val monoidList: Monoid[List[_]] = new Monoid[List[_]] {
    override def empty: List[_] = List.empty

    override def combine(list1: List[_], list2: List[_]): List[_] = list1 ::: list2
  }
}
